﻿using Smod2.EventHandlers;
using Smod2.Events;
using Smod2;
using Smod2.API;
using Smod2.EventSystem.Events;
using System;

namespace Console_IG_Log
{
    internal class RoundHandler : IEventHandlerIntercom, IEventHandlerTeamRespawn, IEventHandlerRoundStart, IEventHandlerRoundEnd, IEventHandlerLCZDecontaminate, IEventHandlerWarheadStartCountdown, IEventHandlerWarheadStopCountdown, IEventHandlerWarheadDetonate, IEventHandlerDecideTeamRespawnQueue
    {
        private IConfigFile cf = ConfigManager.Manager.Config;
        private ConsoleIG consoleIG;

        public RoundHandler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (cf.GetBoolValue("lig_log_decide_team_respawn", true) == true)
            {
                string team = string.Join(", ", ev.Teams);
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onTeamRespawnQueue");
                        msg = msg.Replace("[queue]", team);
                        p.SendConsoleMessage(msg, "blue");
                    }
                }
            }
        }

        public void OnDecontaminate()
        {
            if (cf.GetBoolValue("lig_log_lczdecont", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(consoleIG.GetTranslation("onDecont"), "red");
                    }
                }
            }
        }

        public void OnDetonate()
        {
            if (cf.GetBoolValue("lig_log_warhead_explode", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(consoleIG.GetTranslation("onWarhead_detonate"), "red");
                    }
                }
            }
        }

        public void OnIntercom(PlayerIntercomEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("lig_log_intercom", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onIntercom");
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("([steamid])", "");
                        p.SendConsoleMessage(msg, "blue");
                    }
                }
            }
        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            if(cf.GetBoolValue("lig_log_roundend", true) == true)
            {
                foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        TimeSpan tround = TimeSpan.FromSeconds(ev.Round.Duration);

                        string msg1 = consoleIG.GetTranslation("onRoundEnd_1");

                        string msg2 = consoleIG.GetTranslation("onRoundEnd_2");
                        msg2 = msg2.Replace("[number]", ev.Round.Stats.ClassDEscaped.ToString());

                        string msg3 = consoleIG.GetTranslation("onRoundEnd_3");
                        msg3 = msg3.Replace("[alive]", ev.Round.Stats.ClassDAlive.ToString());
                        msg3 = msg3.Replace("[start]", ev.Round.Stats.ClassDStart.ToString());

                        string msg4 = consoleIG.GetTranslation("onRoundEnd_4");
                        msg4 = msg4.Replace("[number]", ev.Round.Stats.ScientistsEscaped.ToString());

                        string msg5 = consoleIG.GetTranslation("onRoundEnd_5");
                        msg5 = msg5.Replace("[alive]", ev.Round.Stats.ScientistsAlive.ToString());
                        msg5 = msg5.Replace("[start]", ev.Round.Stats.ScientistsStart.ToString());

                        string msg6 = consoleIG.GetTranslation("onRoundEnd_6");
                        msg6 = msg6.Replace("[alive]", ev.Round.Stats.SCPAlive.ToString());
                        msg6 = msg6.Replace("[start]", ev.Round.Stats.SCPStart.ToString());

                        string msg7 = consoleIG.GetTranslation("onRoundEnd_7");
                        msg7 = msg7.Replace("[number]", ev.Round.Stats.SCPKills.ToString());

                        string msg8 = consoleIG.GetTranslation("onRoundEnd_8");
                        msg8 = msg8.Replace("[number]", ev.Round.Stats.GrenadeKills.ToString());

                        string msg9 = consoleIG.GetTranslation("onRoundEnd_9");
                        msg9 = msg9.Replace("[alive]", ev.Round.Stats.NTFAlive.ToString());

                        string msg10 = consoleIG.GetTranslation("onRoundEnd_10");
                        msg10 = msg10.Replace("[time]", tround.ToString());

                        string msg11 = consoleIG.GetTranslation("onRoundEnd_11");


                        

                        p.SendConsoleMessage(msg1);
                        p.SendConsoleMessage(msg2);
                        p.SendConsoleMessage(msg3);
                        p.SendConsoleMessage(msg4);
                        p.SendConsoleMessage(msg5);
                        p.SendConsoleMessage(msg6);
                        p.SendConsoleMessage(msg7);
                        p.SendConsoleMessage(msg8);
                        p.SendConsoleMessage(msg9);
                        p.SendConsoleMessage(msg10);
                        p.SendConsoleMessage(msg11);
                    }
                }
            }
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if(cf.GetBoolValue("lig_log_roundstart", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(consoleIG.GetTranslation("onRoundStart"), "green");
                    }
                }
            }
        }

        public void OnStartCountdown(WarheadStartEvent ev)
        {
            if(cf.GetBoolValue("lig_log_warhead_start", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onWarhead_start");
                        msg = msg.Replace("[activator_role]", consoleIG.GetTranslation(ev.Activator.TeamRole.Role.ToString()));
                        msg = msg.Replace("[activator_name]", ev.Activator.Name);
                        msg = msg.Replace("([steamid])", "");
                        msg = msg.Replace("[time_left]", ev.TimeLeft.ToString());
                        p.SendConsoleMessage(msg, "yellow");
                    }
                }
            }
        }

        public void OnStopCountdown(WarheadStopEvent ev)
        {
            if (cf.GetBoolValue("lig_log_warhead_stop", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onWarhead_stop");
                        msg = msg.Replace("[activator_role]", consoleIG.GetTranslation(ev.Activator.TeamRole.Role.ToString()));
                        msg = msg.Replace("[activator_name]", ev.Activator.Name);
                        msg = msg.Replace("([steamid])", "");
                        p.SendConsoleMessage(msg, "yellow");
                    }
                }
            }
        }

        public void OnTeamRespawn(TeamRespawnEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("lig_log_team_respawn", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if (ev.SpawnChaos == true)
                        {
                            p.SendConsoleMessage(consoleIG.GetTranslation("onTeamRespawn_CI"), "green");
                        }
                        else
                        {
                            p.SendConsoleMessage(consoleIG.GetTranslation("onTeamRespawn_MTF"), "green");
                        }
                    }
                }
            }
        }
    }
}