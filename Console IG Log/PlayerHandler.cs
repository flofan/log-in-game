﻿using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.API;
using System.Linq;

namespace Console_IG_Log
{
    internal class PlayerHandler : IEventHandlerPlayerDie, IEventHandlerLure, IEventHandlerThrowGrenade, IEventHandlerAdminQuery, IEventHandlerBan, IEventHandlerPocketDimensionDie, IEventHandlerPocketDimensionEnter, IEventHandlerPocketDimensionExit, IEventHandlerContain106, IEventHandlerHandcuffed, IEventHandlerElevatorUse, IEventHandlerCheckEscape, IEventHandlerDoorAccess, IEventHandlerAuthCheck, IEventHandlerSpawn, IEventHandlerRecallZombie, IEventHandlerCallCommand, IEvent
 
    {

        private IConfigFile cf = ConfigManager.Manager.Config;
        private ConsoleIG consoleIG;
        private Server server = PluginManager.Manager.Server;

        public PlayerHandler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void OnAdminQuery(AdminQueryEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_command", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.Query != "REQUEST_DATA PLAYER_LIST SILENT")
                        {
                            string msg = consoleIG.GetTranslation("onAdminQuery");
                            msg = msg.Replace("[user_group]", ev.Admin.GetUserGroup().Name);
                            msg = msg.Replace("[user_name]", ev.Admin.Name);
                            msg = msg.Replace("[query]", ev.Query);
                            
                            p.SendConsoleMessage(msg, "yellow");
                        }
                    }
                }
            }
        }

        public void OnAuthCheck(AuthCheckEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_authcheck", true) == true)
            {
                foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        if (ev.Allow == false)
                        {
                            string msg = consoleIG.GetTranslation("onAuthCheck_deny");
                            msg = msg.Replace("[name]", ev.Requester.Name);
                            
                            p.SendConsoleMessage(msg, "red");
                        }
                        else
                        {
                            string msg = consoleIG.GetTranslation("onAuthCheck_allow");
                            msg = msg.Replace("[user_group]", ev.Requester.GetUserGroup().Name);
                            msg = msg.Replace("[user_name]", ev.Requester.Name);
                            
                            p.SendConsoleMessage(msg, "yellow");
                        }
                       
                    }
                    
                }
            }
        }
        public void OnBan(BanEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_ban", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.Reason.Length > 0)
                        {
                            string msg = consoleIG.GetTranslation("onBan_Reason");
                            msg = msg.Replace("[victim_name]", ev.Player.Name);
                            msg = msg.Replace("[admin_name]", ev.Admin.Name);
                            msg = msg.Replace("[admin_group]", ev.Admin.GetUserGroup().Name);
                            msg = msg.Replace("([admin_steamid])", "");
                            msg = msg.Replace("[reason]", ev.Reason);
                            msg = msg.Replace("[duration]", ev.Duration.ToString());
                            p.SendConsoleMessage(msg, "red");
                        }
                        else
                        {
                            string msg = consoleIG.GetTranslation("onBan_NoReason");
                            msg = msg.Replace("[victim_name]", ev.Player.Name);
                            msg = msg.Replace("[admin_name]", ev.Admin.Name);
                            msg = msg.Replace("[admin_group]", ev.Admin.GetUserGroup().Name);
                            msg = msg.Replace("([admin_steamid])", "");
                            msg = msg.Replace("[duration]", ev.Duration.ToString());
                            p.SendConsoleMessage(msg, "red");
                        }
                    }
                }
            }
        }

        public void OnCallCommand(PlayerCallCommandEvent ev)
        {
            if(cf.GetBoolValue("lig_log_player_callcommand", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onCallCommand");
                        msg = msg.Replace("[name]", ev.Player.Name);
                        msg = msg.Replace("[group]", ev.Player.GetUserGroup().Name);
                        msg = msg.Replace("[command]", ev.Command);
                        

                        p.SendConsoleMessage(msg);
                    }
                }
            }
        }

        public void OnCheckEscape(PlayerCheckEscapeEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_escape", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.AllowEscape == true)
                        {
                            string msg = consoleIG.GetTranslation("OnEscape");
                            msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                            msg = msg.Replace("[player_name]", ev.Player.Name);
                            
                            p.SendConsoleMessage(msg, "purple");
                        }
                    }
                }
            }
        }

        public void OnContain106(PlayerContain106Event ev)
        {
            if (cf.GetBoolValue("lig_log_contain_106", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onContain106");
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        
                        p.SendConsoleMessage(msg, "yellow");
                    }
                }
            }
        }

        public void OnDoorAccess(PlayerDoorAccessEvent ev)
        {
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if (cf.GetBoolValue("lig_log_player_door", false) == true)
                        {
                            if (ev.Allow == true && ev.Door.Name.Length <= 0)
                            {
                                string msg = consoleIG.GetTranslation("onDoorAccess_allow");
                                msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                                msg = msg.Replace("[player_name]", ev.Player.Name);
                                
                                p.SendConsoleMessage(msg, "blue");
                            }
                            else if (ev.Allow == false && ev.Door.Name.Length <= 0)
                            {
                                string msg = consoleIG.GetTranslation("onDoorAccess_deny");
                                msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                                msg = msg.Replace("[player_name]", ev.Player.Name);
                                
                                p.SendConsoleMessage(msg, "blue");
                            }
                        }
                        if (cf.GetBoolValue("lig_log_player_door_secure", true) == true)
                        {
                            if (ev.Allow == true && ev.Door.Name.Length > 0)
                            {
                                string msg = consoleIG.GetTranslation("onDoorAccess_secure_allow");
                                msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                                msg = msg.Replace("[player_name]", ev.Player.Name);
                                msg = msg.Replace("[door]", ev.Door.Name);
                                
                                p.SendConsoleMessage(msg, "blue");
                            }
                            else if (ev.Allow == false && ev.Door.Name.Length > 0)
                            {
                                string msg = consoleIG.GetTranslation("onDoorAccess_secure_deny");
                                msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                                msg = msg.Replace("[player_name]", ev.Player.Name);
                                msg = msg.Replace("[door]", ev.Door.Name);
                                
                                p.SendConsoleMessage(msg, "blue");
                            }
                        }
                            
                    }
                }
            }
        }

        public void OnElevatorUse(PlayerElevatorUseEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_elevator", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onElevator");
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        msg = msg.Replace("[name]", $"({ev.Elevator.ElevatorType.ToString()})");
                        p.SendConsoleMessage(msg, "yellow"); 
                    }
                }
            }
        }

        public void OnHandcuffed(PlayerHandcuffedEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_handcuff", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.Handcuffed == true)
                        {
                            string msg = consoleIG.GetTranslation("onHandcuff");
                            msg = msg.Replace("[player_name]", ev.Owner.Name);
                            msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Owner.TeamRole.Role.ToString()));
                            msg = msg.Replace("[victim_name]", ev.Player.Name);
                            msg = msg.Replace("[victim_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                            
                            p.SendConsoleMessage(msg, "yellow");
                        }
                    }
                }
            }
        }

        public void OnLure(PlayerLureEvent ev)
        {
            if (cf.GetBoolValue("lig_log_lure", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onLure");
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        
                        p.SendConsoleMessage(msg, "yellow");

                    }
                }
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            int[] teamNTF = { (int)Team.MTF, (int)Team.RSC }, teamChaos = { (int)Team.CHI, (int)Team.CDP };

            Player v = ev.Player;
            Player k = ev.Killer;

            if (v.Name == "Server" || k.Name == "Server" || v.Name == string.Empty || k.Name == string.Empty || v.Name == "Dedicated Server" || k.Name == "Dedicated Server" || k.Name == "World") { return; }
            bool checkteamkill() {
                if (teamNTF.Contains((int)v.TeamRole.Team) && teamNTF.Contains((int)k.TeamRole.Team))
                    return true;
                if (teamChaos.Contains((int)v.TeamRole.Team) && teamChaos.Contains((int)k.TeamRole.Team))
                    return true;
                return false;
            }

            
            foreach (Player p in PluginManager.Manager.Server.GetPlayers())
            {
                if(perm.CheckPerm(p) == true)
                {
                    if (v.Name == "Server" || k.Name == "Server" || v.Name == string.Empty || k.Name == string.Empty) { ev.SpawnRagdoll = false; return; }
                    if (cf.GetBoolValue("lig_log_player_kill", true) == true)
                    {
                        if(checkteamkill() == true)
                        {
                            string msg = consoleIG.GetTranslation("onDie_TeamKill");
                            msg = msg.Replace("[killer_role]", consoleIG.GetTranslation(k.TeamRole.Role.ToString()));
                            msg = msg.Replace("[killer_name]", k.Name);
                            msg = msg.Replace("([killer_steamid])", "");
                            msg = msg.Replace("[player_role]", consoleIG.GetTranslation(v.TeamRole.Role.ToString()));
                            msg = msg.Replace("[player_name]", v.Name);
                            msg = msg.Replace("([player_steamid])", "");
                            msg = msg.Replace("[damage]", consoleIG.GetTranslation(ev.DamageTypeVar.ToString()));
                            p.SendConsoleMessage(msg, "red");
                        }
                        else if(!(ev.Player.SteamId == ev.Killer.SteamId))
                        {
                            string msg = consoleIG.GetTranslation("onDie_Kill");
                            msg = msg.Replace("[killer_role]", consoleIG.GetTranslation(k.TeamRole.Role.ToString()));
                            msg = msg.Replace("[killer_name]", k.Name);
                            msg = msg.Replace("([killer_steamid])", "");
                            msg = msg.Replace("[player_role]", consoleIG.GetTranslation(v.TeamRole.Role.ToString()));
                            msg = msg.Replace("[player_name]", v.Name);
                            msg = msg.Replace("([player_steamid])", "");
                            msg = msg.Replace("[damage]", consoleIG.GetTranslation(ev.DamageTypeVar.ToString()));
                            p.SendConsoleMessage(msg);
                        }
                        else if (k.SteamId == v.SteamId)
                        {
                            string msg = consoleIG.GetTranslation("onDie_Suicid");
                            msg = msg.Replace("[killer_role]", consoleIG.GetTranslation(v.TeamRole.Role.ToString()));
                            msg = msg.Replace("[killer_name]", v.Name);
                            msg = msg.Replace("([killer_steamid])", "");
                            msg = msg.Replace("[damage]", consoleIG.GetTranslation(ev.DamageTypeVar.ToString()));
                            p.SendConsoleMessage(msg);
                        }
                    }
                }
                
            }
        }

        public void OnPocketDimensionDie(PlayerPocketDimensionDieEvent ev)
        {
            if(cf.GetBoolValue("lig_log_pd_die", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onPd_die");
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        
                        p.SendConsoleMessage(msg, "green");
                    }
                }
            }
        }

        public void OnPocketDimensionEnter(PlayerPocketDimensionEnterEvent ev)
        {
            if (cf.GetBoolValue("lig_log_pd_enter", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onPd_enter");

                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        
                        p.SendConsoleMessage(msg, "green");
                    }
                }
            }
        }

        public void OnPocketDimensionExit(PlayerPocketDimensionExitEvent ev)
        {
            if (cf.GetBoolValue("lig_log_pd_exit", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onPd_exit");

                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        
                        p.SendConsoleMessage(msg, "green");
                    }
                }
            }
        }

        public void OnRecallZombie(PlayerRecallZombieEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_zombie", true))
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onRecallZombie");
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        
                        msg = msg.Replace("[victim_name]", ev.Target.Name);
                        msg = msg.Replace("([victim_steamid])", "");

                        p.SendConsoleMessage(msg, "yellow");
                    }
                }
            }
        }
        public void OnSpawn(PlayerSpawnEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_spawn", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onSpawn");
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        msg = msg.Replace("[role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        
                        p.SendConsoleMessage(msg);
                    }
                }
            }

        }

        public void OnThrowGrenade(PlayerThrowGrenadeEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_grenade", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if(ev.GrenadeType.Equals(ItemType.FLASHBANG))
                    {
                        string msg = consoleIG.GetTranslation("onGrenade_flash");
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        
                        p.SendConsoleMessage(msg, "yellow");
                    }
                    else
                    {
                        string msg = consoleIG.GetTranslation("onGrenade_frag");
                        msg = msg.Replace("[player_role]", consoleIG.GetTranslation(ev.Player.TeamRole.Role.ToString()));
                        msg = msg.Replace("[player_name]", ev.Player.Name);
                        
                        p.SendConsoleMessage(msg, "yellow");
                    }
                }
            }
        }
    }
}